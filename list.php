<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style type="text/css">
        .modal-header, h2, .close {
              background-color: #5cb85c;
              color:white !important;
              text-align: center;
              font-size: 30px;
          }
          .modal-footer {
              background-color: #f9f9f9;
          }
        </style>
    </head>
    <body>
        <div class="container-fluid container-fluid-1">
            <nav class="navbar navbar-inverse">
              <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand" href="index.php">Airbnb</a>
                </div>
                <!--<ul class="nav navbar-nav">
                  <li class="active"><a href="#">Home</a></li>
                  <li><a href="#">Page 1</a></li>
                  <li><a href="#">Page 2</a></li>
                </ul>-->
                <form class="navbar-form navbar-left" action="#">
                  <div class="input-group">
                    <input type="text" class="form-control location" placeholder="Search" name="search" value="<?php echo $_POST["city"]; ?>">
                    <div class="input-group-btn">
                      <button class="btn btn-default" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                      </button>
                    </div>
                  </div>
                </form>
                <ul class="nav navbar-nav navbar-right">
                  <!--<li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>-->
                  <li><a href="#">Become Host</a></li>
                  <li><a href="#">Get Hosting Help</a></li>
                  <li><a href="#">Saved</a></li>
                  <li><a href="#">Trips</a></li>
                  <li><a href="#">Message</a></li>
                  <li>  <button type="button" class="btn btn-default btn-sm logbtn" id="myBtn"><span class="glyphicon glyphicon-user"></span> Login</button>
                  </li>
                </ul>
              </div>
            </nav>
        </div>
        <div class="container-fluid">
            <!--<div class="row bookingbar">
                <div class="col-md-2">
                    <h3><strong>Your Are Searching For :</strong></h3>
                </div>
                <div class="col-md-2">
                    <label>Location :</label>
                    <input type="text" class="form-control location" value="<?php echo $_POST["city"]; ?>">
                </div>
                <div class="col-md-2">
                    <label>Checkin :</label>
                    <input type="text" class="form-control" value="<?php echo $_POST["checkin"]; ?>">
                </div>
                <div class="col-md-2">
                    <label>Checkout :</label>
                    <input type="text" class="form-control" value="<?php echo $_POST["checkout"]; ?>">
                </div>
                <div class="col-md-2">
                    <label>Adult :</label>
                    <input type="text" class="form-control" value="<?php echo $_POST["adult"]; ?>">
                </div>
                <div class="col-md-2">
                    <label>Children :</label>
                    <input type="text" class="form-control" value="<?php echo $_POST["child"]; ?>">
                </div>
            </div>-->

            <!-- filter goes here-->
            <hr>
            <div class="row">
                <div class="col-md-9">
                    <button type="button" class="btn btn-sm btn-default">Date</button>
                    <button type="button" class="btn btn-sm btn-default">1 Guest</button>
                    <button type="button" class="btn btn-sm btn-default">Home Type</button>
                    <button type="button" class="btn btn-sm btn-default">Price</button>
                    <button type="button" class="btn btn-sm btn-default">Instant Book</button>
                    <button type="button" class="btn btn-sm btn-default">More Filter</button>
                </div>
                <div class="col-md-3">
                    <button type="button" class="btn btn-sm btn-default pull-right">5 Recently Visited Home</button>
                </div>
            </div>
            <hr>
            <!-- filter row closed here-->

            <div class="row">
                <div class="col-md-8 hotel">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#">
                                <img src="images/im1.jpeg" class="img-responsive">
                                <p><strong>Full House /Apt</strong></p>
                                <h4><strong>Heritage Apt 1@Hauz Khas Village</strong></h4>
                                <h5><strong>$ 50 / Night | Free Cancelation</strong></h5>
                                <div class="col-md-12">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="#">
                                <img src="images/im2.jpeg" class="img-responsive">
                                <p><strong>Full House /Apt</strong></p>
                                <h4><strong>Heritage Apt 1@Hauz Khas Village</strong></h4>
                                <h5><strong>$ 50 / Night | Free Cancelation</strong></h5>
                                <div class="col-md-12">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row travel">
                        <div class="col-md-6">
                            <a href="#">
                                <img src="images/im4.jpeg" class="img-responsive">
                                <p><strong>Full House /Apt</strong></p>
                                <h4><strong>Heritage Apt 1@Hauz Khas Village</strong></h4>
                                <h5><strong>$ 50 / Night | Free Cancelation</strong></h5>
                                <div class="col-md-12">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="#">
                                <img src="images/im5.jpeg" class="img-responsive">
                                <p><strong>Full House /Apt</strong></p>
                                <h4><strong>Heritage Apt 1@Hauz Khas Village</strong></h4>
                                <h5><strong>$ 50 / Night | Free Cancelation</strong></h5>
                                <div class="col-md-12">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row travel">
                        <div class="col-md-6">
                            <a href="#">
                                <img src="images/im1.jpeg" class="img-responsive">
                                <p><strong>Full House /Apt</strong></p>
                                <h4><strong>Heritage Apt 1@Hauz Khas Village</strong></h4>
                                <h5><strong>$ 50 / Night | Free Cancelation</strong></h5>
                                <div class="col-md-12">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="#">
                                <img src="images/im2.jpeg" class="img-responsive">
                                <p><strong>Full House /Apt</strong></p>
                                <h4><strong>Heritage Apt 1@Hauz Khas Village</strong></h4>
                                <h5><strong>$ 50 / Night | Free Cancelation</strong></h5>
                                <div class="col-md-12">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row travel">
                        <div class="col-md-6">
                            <a href="#">
                                <img src="images/im4.jpeg" class="img-responsive">
                                <p><strong>Full House /Apt</strong></p>
                                <h4><strong>Heritage Apt 1@Hauz Khas Village</strong></h4>
                                <h5><strong>$ 50 / Night | Free Cancelation</strong></h5>
                                <div class="col-md-12">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="#">
                                <img src="images/im5.jpeg" class="img-responsive">
                                <p><strong>Full House /Apt</strong></p>
                                <h4><strong>Heritage Apt 1@Hauz Khas Village</strong></h4>
                                <h5><strong>$ 50 / Night | Free Cancelation</strong></h5>
                                <div class="col-md-12">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <iframe src="https://www.google.com/maps/d/embed?mid=15icUPhyQI6ouDg4J9e_JpSqb-4P3aVy9&hl=en" width="100%" height="580"></iframe>
                </div>
            </div>
        </div>















        <div class="container-fluid">
            <hr>
        </div>
        <!--footer start here-->
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <select class="form-control">
                        <option>Language</option>
                        <option>Hindi</option>
                        <option>English</option>
                        <option>French</option>
                        <option>German</option>
                        <option>Nepali</option>
                    </select>
                    <br>
                    <select class="form-control">
                        <option>Currency</option>
                        <option>INR</option>
                        <option>DOLLOR</option>
                        <option>RINGGIT</option>
                        <option>YEN</option>
                    </select>
                </div>
                <div class="col-md-3 text-center">
                    <h4><strong>Airbnb</strong></h4>
                    <ul>
                        <li class="hello"><a href="#">Career</a></li>
                        <li class="hello"><a href="#">Press</a></li>
                        <li class="hello"><a href="#">Policy</a></li>
                        <li class="hello"><a href="#">Help</a></li>
                        <li class="hello"><a href="#">Divercity</a></li>
                    </ul>
                </div>
                <div class="col-md-3 text-center">
                    <h4><strong>Discover</strong></h4>
                    <ul>
                        <li class="hello"><a href="#">Career</a></li>
                        <li class="hello"><a href="#">Press</a></li>
                        <li class="hello"><a href="#">Policy</a></li>
                        <li class="hello"><a href="#">Help</a></li>
                        <li class="hello"><a href="#">Divercity</a></li>
                    </ul>
                </div>
                <div class="col-md-3 text-center">
                    <h4><strong>Hosting</strong></h4>
                    <ul>
                        <li class="hello"><a href="#">Career</a></li>
                        <li class="hello"><a href="#">Press</a></li>
                        <li class="hello"><a href="#">Policy</a></li>
                        <li class="hello"><a href="#">Help</a></li>
                        <li class="hello"><a href="#">Divercity</a></li>
                    </ul>
                </div>
            </div>


            <hr>


            <div class="row travel foot">
                <div class="col-md-4">
                    &copy Airbnb, Inc.
                </div>
                <div class="col-md-8">
                    <div class="pull-right">
                        <a href="#">Term</a>
                        <span>|</span>
                        <a href="#">Privacy</a>
                        <span>|</span>
                        <a href="#">Site Map</a>
                        <span>|</span>
                        <a href="#"><img src="images/facebook.png">
                        <span>|</span>
                        <a href="#"><img src="images/twitter.png">
                        <span>|</span>
                        <a href="#"><img src="images/instagram.png">
                    </div>
                </div>
            </div>
        </div>

        <!--footer end here-->






        <!-- login model goes here-->
         <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2><span class="glyphicon glyphicon-lock"></span> Login</h2>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form">
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-user"></span> Username</label>
              <input type="text" class="form-control" id="usrname" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
              <input type="text" class="form-control" id="psw" placeholder="Enter password">
            </div>
            <div class="checkbox">
              <label><input type="checkbox" value="" checked>Remember me</label>
            </div>
              <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-off"></span> Login</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
          <p>Not a member? <a href="#">Sign Up</a></p>
          <p>Forgot <a href="#">Password?</a></p>
        </div>
      </div>

    </div>
  </div>

<script>
$(document).ready(function(){
    $("#myBtn").click(function(){
        $("#myModal").modal();
    });
});
</script>
        <!-- login model closed here-->
    </body>
</html>