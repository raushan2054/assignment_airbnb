<!DOCTYPE html>
<html>
    <head>
        <title>Task To Be Completed Within This Week.</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <!--main container start here for image background-->
        <div class="container-fluid main">
            <!--logo div goes here-->
            <div>

            </div>
            <!--logo div closed here-->

            <!--form div goes here-->
            <div class="row">
                <div class="col-md-4 formback">
                    <h1><b>Finds Home On Airbnb</b></h1>
                    <h4>Discover entire homes and private rooms perfect for any trip.</h4>
                    <form method="post" action="list.php">
                        <div class="row">
                            <div class="col-md-12">
                                <label>WHERE</label>
                                <input type="text" name="city" class="form-control" placeholder="Anywhere" required>
                            </div>
                            <br>
                            <div class="col-md-6">
                                <label>CHECK-IN</label>
                                <input type="date" name="checkin" class="form-control" required>
                            </div>
                            <div class="col-md-6">
                                <label>CHECK-OUT</label>
                                <input type="date" name="checkout" class="form-control" required>
                            </div>
                            <br>
                            <div class="col-md-6">
                                <label>ADULT</label>
                                <select class="form-control" name="adult" required>
                                    <option></option>
                                    <option>1 ADULT</option>
                                    <option>2 ADULT</option>
                                    <option>3 ADULT</option>
                                    <option>4 ADULT</option>
                                    <option>5 ADULT</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>CHILDREN</label>
                                <select class="form-control" name="child" required>
                                    <option></option>
                                    <option>1 CHILDREN</option>
                                    <option>2 CHILDREN</option>
                                    <option>3 CHILDREN</option>
                                    <option>4 CHILDREN</option>
                                    <option>5 CHILDREN</option>
                                </select>
                            </div>
                            <br>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-lg btn-danger btn-block" value="Search" name="submit">
                            </div>
                        </div>
                        <br><br>
                    </form>
                </div>
                <div class="col-md-8">

                </div>
            </div>
            <!--form div closed here-->
        </div>
        <!--main container closed here with image background-->



        <!-- second narrow container start from here-->
        <div class="container">
            <div class="row">
                <div class="col-md-12 travel">
                    <h3><strong>What guests are saying about homes in India</strong></h3>
                </div>
                <div class="col-md-1">
                    <img src="images/key.png">
                </div>
                <div class="col-md-5">
                    <h1>160,000+ reviews <br><small>of Airbnb homes in India</small></h1>
                </div>
                <div class="col-md-1">
                    <img src="images/trophy.png">
                </div>
                <div class="col-md-5">
                <h1>4.6 out of 5 stars <br><small>average home rating in India</small></h1>
                </div>
            </div>
            <!-- image row goes here-->
            <div class="row">
                <div class="col-md-4">
                    <a href="#">
                        <img src="images/im4.jpeg" class="img-responsive">
                        <div class="col-md-12 star">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                        </div>
                        <p>
                            Anita extended a warm welcome to us. Chill out areas, nice people, the close distance to the sea give the accommodation a great atmosphere.
                        </p>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="images/av2.png" class="img-responsive img-circle">
                            </div>
                            <div class="col-md-10">
                                <strong>Raushan kumar</strong>
                                <br>
                                <small>New Delhi</small>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="#">
                        <img src="images/im1.jpeg" class="img-responsive">
                        <div class="col-md-12 star">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                        </div>
                        <p>
                            Anita extended a warm welcome to us. Chill out areas, nice people, the close distance to the sea give the accommodation a great atmosphere.
                        </p>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="images/av1.png" class="img-responsive img-circle">
                            </div>
                            <div class="col-md-10">
                                <strong>Raushan kumar</strong>
                                <br>
                                <small>New Delhi</small>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="#">
                        <img src="images/im2.jpeg" class="img-responsive">
                        <div class="col-md-12 star">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                        </div>
                        <p>
                            Anita extended a warm welcome to us. Chill out areas, nice people, the close distance to the sea give the accommodation a great atmosphere.
                        </p>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="images/av3.png" class="img-responsive img-circle">
                            </div>
                            <div class="col-md-10">
                                <strong>Raushan kumar</strong>
                                <br>
                                <small>New Delhi</small>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <!-- image row closed here-->






            <div class="row travel">
                <div class="col-md-12">
                    <h3><strong>Traveling with Airbnb</strong></h3>
                </div>
            </div>
            <div class="row travel">
                <div class="col-md-4">
                    <img src="images/heart.png" class="img-responsive">
                    <p><h4><strong>24/7 customer support</strong></h4></p>
                    <p>
                        Day or night, we’re here for you. Talk to our support team from anywhere in the world, any hour of day.
                    </p>
                </div>
                <div class="col-md-4">
                    <img src="images/hut.png" class="img-responsive">
                    <p><h4><strong>Global hospitality standards</strong></h4></p>
                    <p>
                        Guests review their hosts after each stay. All hosts must maintain a minimum rating and our hospitality standards to be on Airbnb.
                    </p>
                </div>
                <div class="col-md-4">
                    <img src="images/user.png" class="img-responsive">
                    <p><h4><strong>5-star hosts</strong></h4></p>
                    <p>From fresh-pressed sheets to tips on where to get the best brunch, our hosts are full of local hospitality.</p>
                </div>
            </div>








            <div class="row travel">
                <div class="col-md-12">
                    <h3><strong>Homes</strong></h3>
                </div>
            </div>
            <div class="row travel">
                <div class="col-md-3">
                    <a href="#">
                        <img src="images/im5.jpeg" class="img-responsive img-rounded">
                        <p><strong>Entire Home/Apt : 2 Beds</strong></p>
                        <h4><strong>YOUR PRIVATE 3 BEDR. RIAD, AN EXCLUSIVE </strong></h4>
                        <p><strong>$ 125 / Night</strong></p>
                        <div class="like">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span>1130</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="#">
                        <img src="images/im4.jpeg" class="img-responsive img-rounded">
                        <p><strong>Entire Home/Apt : 2 Beds</strong></p>
                        <h4>
                            <strong>Mushroom Dome Cabin: #1 on airbnb in the world</strong>
                        </h4>
                        <p><strong>$ 125 / Night</strong></p>
                        <div class="like">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span>1130</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="#">
                        <img src="images/im1.jpeg" class="img-responsive img-rounded">
                        <p><strong>Entire Home/Apt : 2 Beds</strong></p>
                        <h4><strong>House in countryside (30 km dal mare)</strong></h4>
                        <p><strong>$ 125 / Night</strong></p>
                        <div class="like">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span>1130</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <img src="images/im2.jpeg" class="img-responsive img-rounded">
                </div>
            </div>












            <div class="row travel">
                <div class="col-md-9">
                    <h2><strong>When are you traveling?</strong></h2>
                    <h4><strong>Add dates for updated pricing and availability.</strong></h4>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-primary btn-lg btn-block">Add Date</button>
                </div>
            </div>
        </div>
        <!-- second narrow container enad from here-->



        <div class="container-fluid">
            <hr>
        </div>
        <!--footer start here-->
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <select class="form-control">
                        <option>Language</option>
                        <option>Hindi</option>
                        <option>English</option>
                        <option>French</option>
                        <option>German</option>
                        <option>Nepali</option>
                    </select>
                    <br>
                    <select class="form-control">
                        <option>Currency</option>
                        <option>INR</option>
                        <option>DOLLOR</option>
                        <option>RINGGIT</option>
                        <option>YEN</option>
                    </select>
                </div>
                <div class="col-md-3 text-center">
                    <h4><strong>Airbnb</strong></h4>
                    <ul>
                        <li class="hello"><a href="#">Career</a></li>
                        <li class="hello"><a href="#">Press</a></li>
                        <li class="hello"><a href="#">Policy</a></li>
                        <li class="hello"><a href="#">Help</a></li>
                        <li class="hello"><a href="#">Divercity</a></li>
                    </ul>
                </div>
                <div class="col-md-3 text-center">
                    <h4><strong>Discover</strong></h4>
                    <ul>
                        <li class="hello"><a href="#">Career</a></li>
                        <li class="hello"><a href="#">Press</a></li>
                        <li class="hello"><a href="#">Policy</a></li>
                        <li class="hello"><a href="#">Help</a></li>
                        <li class="hello"><a href="#">Divercity</a></li>
                    </ul>
                </div>
                <div class="col-md-3 text-center">
                    <h4><strong>Hosting</strong></h4>
                    <ul>
                        <li class="hello"><a href="#">Career</a></li>
                        <li class="hello"><a href="#">Press</a></li>
                        <li class="hello"><a href="#">Policy</a></li>
                        <li class="hello"><a href="#">Help</a></li>
                        <li class="hello"><a href="#">Divercity</a></li>
                    </ul>
                </div>
            </div>


            <hr>


            <div class="row travel foot">
                <div class="col-md-4">
                    &copy Airbnb, Inc.
                </div>
                <div class="col-md-8">
                    <div class="pull-right">
                        <a href="#">Term</a>
                        <span>|</span>
                        <a href="#">Privacy</a>
                        <span>|</span>
                        <a href="#">Site Map</a>
                        <span>|</span>
                        <a href="#"><img src="images/facebook.png">
                        <span>|</span>
                        <a href="#"><img src="images/twitter.png">
                        <span>|</span>
                        <a href="#"><img src="images/instagram.png">
                    </div>
                </div>
            </div>
        </div>

        <!--footer end here-->
    </body>
</html>